
public class Conta {
	//Atributos
	protected double saldo;
	private String cpf, nome;
	private String numConta;
	
	//Construtor
	public Conta (){
		
	}
	public Conta(double saldo){
		this.setSaldo(saldo);
	}
	
	public Conta(String nome){
		this.nome = nome;
	}
	
	public Conta(String nome, String cpf, String numConta){
		this.nome = nome;
		this.cpf = cpf;
		this.numConta = numConta;
	}
	
	//Metodos
	public void deposita(double valor){
		this.setSaldo(this.getSaldo() + valor);
	}
	
	public void saca(double valor){
		this.setSaldo(this.getSaldo() - valor);
	}
	
	public double getSaldo(){
		return this.saldo;
	}
	
	void atualiza (double taxa){
		this.setSaldo(this.getSaldo() + this.getSaldo()*taxa);
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

}
