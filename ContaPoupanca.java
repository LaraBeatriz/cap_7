
public class ContaPoupanca extends Conta {
	
	public ContaPoupanca(){
		
	}

	public ContaPoupanca(String nome, String cpf, String numConta) {
		super(nome, cpf, numConta);
		
	}
	
	public void atualiza(double taxa){
		this.saldo += this.saldo *taxa*3;
	}

}
